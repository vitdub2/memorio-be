import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { UserDto, UserState } from '../user/user.interfaces';
import { JwtService } from '@nestjs/jwt';
import { ILogin, IPassword, IToken } from './auth.interfaces';
import { passwordCutter } from '../user/password.cutter';
import * as bcrypt from 'bcrypt';
import { MailService } from '../mail/mail.service';
import { TaskService } from '../task/task.service';
import { PushService } from '../push/push.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly mailService: MailService,
    private readonly taskService: TaskService,
    private readonly pushService: PushService,
  ) {}

  async confirmUser(confirmCode: string): Promise<any> {
    const user = await this.userService.findUserByConfirmCode(confirmCode);
    if (user) {
      const dto = passwordCutter(user);
      await this.userService.regenerateConfirmCode(user['_id'].toString());
      return this.login(dto);
    } else {
      throw new NotFoundException('Неверный код подтверждения');
    }
  }

  async sendChangePasswordMail(loginDto: ILogin): Promise<any> {
    try {
      const user = await this.userService.findUserByLogin(loginDto.login);
      if (user) {
        await this.mailService.sendEmail({
          to: user.email,
          subject: 'Reset password',
          templateName: 'reset_password',
          link: `${user.confirmCode}/reset`,
        });
        return {
          email: user.email,
        };
      }
    } catch (e) {
      throw new NotFoundException('Неверный логин');
    }
  }

  async changePassword(confirmCode: string, password: IPassword): Promise<any> {
    try {
      let user = await this.userService.findUserByConfirmCode(confirmCode);
      if (user) {
        user = await this.userService.setNewPasswordForUser(
          user['_id'].toString(),
          password,
        );
        const dto = passwordCutter(user);
        return this.login(dto);
      }
    } catch (e) {
      throw new NotFoundException('Неверный код сброса пароля');
    }
  }

  async validateUser(login: string, password: string): Promise<UserDto | null> {
    const user = await this.userService.findUserByLogin(login);
    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        if (user.state === UserState.CONFIRMED) {
          await this.createJobsForUser(user);
          return user;
        } else {
          throw new UnauthorizedException('Подтвердите аккаунт через email');
        }
      } else {
        throw new UnauthorizedException('Неверный логин/пароль');
      }
    }
    throw new UnauthorizedException('Неверный логин/пароль');
  }

  async login(user: any): Promise<IToken> {
    if (user['_doc']) {
      user = passwordCutter(user);
    }
    await this.createJobsForUser(user);
    return {
      token: this.jwtService.sign(user),
    };
  }

  async logout(user: any): Promise<any> {
    const id = user['_id'];
    const tasks = await this.taskService.getAllTasksForUser(id);
    this.pushService.deleteCronJobsTaskArray(tasks);
    return user;
  }

  private async createJobsForUser(user: any) {
    const id = user['_id'];
    const tasks = await this.taskService.getAllTasksForUser(id);
    this.pushService.createCronJobsTaskArray(tasks);
  }

  getUserFromToken(token: string) {
    try {
      return this.jwtService.verify(token);
    } catch (e) {
      return {};
    }
  }
}
