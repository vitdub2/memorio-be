import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { LocalAuthGuard } from './local.auth.guard';
import { AuthService } from './auth.service';
import { User } from '../utils/user.decorator';
import { ApiTags } from '@nestjs/swagger';
import { Public } from '../utils/public.decorator';
import { MODULE_NAMES } from '../utils/constants';
import { IDeviceInfo, ILogin, IPassword } from './auth.interfaces';
import { ValidationPipe } from '../utils/validation.pipe';

@Controller(MODULE_NAMES.AUTH)
@ApiTags(MODULE_NAMES.AUTH)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @Public()
  @UseGuards(LocalAuthGuard)
  async login(@User() user) {
    return this.authService.login(user);
  }

  @Get(':confirmCode/confirm')
  @Public()
  async confirmUser(@Param('confirmCode') confirmCode: string): Promise<any> {
    return this.authService.confirmUser(confirmCode);
  }

  @Post('changePassword')
  @Public()
  async sendChangePasswordMail(
    @Body(new ValidationPipe()) loginDto: ILogin,
  ): Promise<any> {
    return this.authService.sendChangePasswordMail(loginDto);
  }

  @Put(':confirmCode/changePassword')
  @Public()
  async changePassword(
    @Param('confirmCode') confirmCode: string,
    @Body(new ValidationPipe()) password: IPassword,
  ): Promise<any> {
    return this.authService.changePassword(confirmCode, password);
  }

  @Post('/logout')
  async logout(@User() user) {
    return this.authService.logout(user);
  }

  @Get('/profile')
  async getAuthorizedUser(@User() user) {
    return user;
  }
}
