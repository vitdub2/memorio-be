import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export interface IToken {
  token: string;
}

export interface IPassword {
  password: string;
}

export interface ILogin {
  login: string;
}

export class IDeviceInfo {
  @IsString()
  @ApiProperty()
  deviceId: string;
}
