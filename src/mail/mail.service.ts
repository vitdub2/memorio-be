import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { IMailSettings } from './mail.interface';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  sendEmail(settings: IMailSettings) {
    const host = process.env.HOST_NAME || `https://motivator-fe5bb.web.app`;
    const port = process.env.PORT || 3000;
    const host_link = host.includes('localhost') ? `${host}:${port}` : host;
    this.mailerService
      .sendMail({
        to: settings.to,
        from: process.env.EMAIL,
        subject: settings.subject,
        template: `src/mail/templates/${settings.templateName}`,
        context: {
          href_link: `${host_link}/#/${settings.link}`,
        },
      })
      .then(() => {
        console.log('Message sent to: ' + settings.to);
      })
      .catch(console.error);
  }
}
