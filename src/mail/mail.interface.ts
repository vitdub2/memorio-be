export interface IMailSettings {
  to: string;
  subject: string;
  templateName: string;
  link: string;
}
