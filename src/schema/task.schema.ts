import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema({ timestamps: true })
export class Task {
  @Prop({ required: true, type: mongoose.SchemaTypes.ObjectId, ref: 'User' })
  userId: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: false })
  desc: string;

  @Prop({ required: true, type: [mongoose.SchemaTypes.String] })
  notificationTimes: string[];
}

export type TaskDocument = Task & mongoose.Document;

export const TaskSchema = SchemaFactory.createForClass(Task);
