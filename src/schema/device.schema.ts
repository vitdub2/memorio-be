import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { DeviceTypes } from '../device/device.interfaces';

@Schema({ timestamps: true })
export class Device {
  @Prop({ required: true, type: mongoose.SchemaTypes.ObjectId, ref: 'User' })
  userId: string;

  @Prop({ required: true })
  token: string;

  @Prop({
    required: true,
    enum: [DeviceTypes.MOBILE, DeviceTypes.DESKTOP, DeviceTypes.TABLET],
  })
  type: string;

  @Prop({ required: true })
  browser: string;

  @Prop({ required: false, default: '' })
  name: string;

  @Prop({ required: true, type: mongoose.SchemaTypes.Boolean, default: true })
  shouldBeNotified: boolean;
}

export type DeviceDocument = Device & mongoose.Document;

export const DeviceSchema = SchemaFactory.createForClass(Device);
