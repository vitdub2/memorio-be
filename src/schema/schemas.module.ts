import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';
import { Task, TaskSchema } from './task.schema';
import { Device, DeviceSchema } from './device.schema';

const schemas = [
  MongooseModule.forFeature([
    { name: User.name, schema: UserSchema },
    { name: Task.name, schema: TaskSchema },
    { name: Device.name, schema: DeviceSchema },
  ]),
];

@Module({
  imports: schemas,
  exports: schemas,
})
export class SchemasModule {}
