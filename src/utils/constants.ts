import * as functions from 'firebase-functions';

export const MODULE_NAMES = {
  USERS: 'users',
  AUTH: 'auth',
  TASKS: 'tasks',
  DEVICES: 'devices',
};

export const MONGO_REPO_OPTIONS = {
  useFindAndModify: false,
  new: true,
};

export const ENV_CONFIG = functions.config();
