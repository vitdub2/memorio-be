import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { DeviceModule } from '../device/device.module';
import { PushService } from './push.service';

@Module({
  imports: [UserModule, DeviceModule],
  providers: [PushService],
  controllers: [],
  exports: [PushService],
})
export class PushModule {}
