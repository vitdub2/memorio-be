import { Injectable } from '@nestjs/common';
import { DeviceService } from '../device/device.service';
import { map, filter, forEach } from 'lodash';
import * as admin from 'firebase-admin';
import { Device } from '../schema/device.schema';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';
import { Task } from '../schema/task.schema';

@Injectable()
export class PushService {
  constructor(
    private readonly deviceService: DeviceService,
    private schedulerRegistry: SchedulerRegistry,
  ) {}

  private static formCronTime(time: string): string {
    const parts = time.split(':');
    const hours = +parts[0];
    const minutes = +parts[1];
    return `0 ${minutes} ${hours} * * 0-6`;
  }

  private static formCronJobName(id: string, time: string) {
    return `${id} - ${time}`;
  }

  async createCronJobsForTask(task: Task) {
    const times = task.notificationTimes;
    const id = task['_id'];
    forEach(times, (time: string) => {
      const name = PushService.formCronJobName(id, time);
      const job = new CronJob(
        PushService.formCronTime(time),
        () => {
          console.log(name + ' performing');
          this.pushTask(task);
        },
        null,
        false,
        'Europe/Moscow',
      );
      if (!this.schedulerRegistry.doesExists('cron', name)) {
        console.log('Cron job created: ' + name);
        this.schedulerRegistry.addCronJob(name, job);
        job.start();
      }
    });
  }

  createCronJobsTaskArray(tasks: Task[]) {
    forEach(tasks, (task: Task) => {
      this.createCronJobsForTask(task);
    });
  }

  deleteCronJobsTaskArray(tasks: Task[]) {
    forEach(tasks, (task: Task) => {
      this.deleteCronJobsForTask(task);
    });
  }

  deleteCronJobsForTask(task: Task) {
    const times = task.notificationTimes;
    const id = task['_id'];
    forEach(times, (time: string) => {
      const jobName = PushService.formCronJobName(id, time);
      if (this.schedulerRegistry.doesExists('cron', jobName)) {
        console.log('Cron job deleted: ' + jobName);
        const job = this.schedulerRegistry.getCronJob(jobName);
        this.schedulerRegistry.deleteCronJob(jobName);
        job.stop();
      }
    });
  }

  async pushTask(task: Task) {
    const userId = task.userId.toString();
    const devices = await this.deviceService.getUserDevices(userId);
    const notifiableDevices = filter(
      devices,
      (device: Device) => device.shouldBeNotified,
    );
    const messages = map(notifiableDevices, (device: Device) => ({
      token: device.token.trim(),
      notification: {
        title: task.name,
        body: task.desc,
      },
    }));
    admin
      .messaging()
      .sendAll(messages)
      .then((response) => {
        // Response is a message ID string.
        if (response.failureCount) {
          console.log(response.responses[0].error);
        }
        console.log('Successfully sent message:', response.successCount);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
  }
}
