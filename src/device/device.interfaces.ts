import { IsBoolean, IsEnum, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Optional } from '@nestjs/common';

export enum DeviceTypes {
  MOBILE = 'MOBILE',
  DESKTOP = 'DESKTOP',
  TABLET = 'TABLET',
}

export class DeviceDto {
  @IsString()
  @ApiProperty()
  token: string;

  @IsEnum(DeviceTypes)
  @ApiProperty()
  type: string;

  @IsString()
  @ApiProperty()
  browser: string;

  @Optional()
  @IsBoolean()
  @ApiProperty()
  shouldBeNotified: boolean;

  @Optional()
  @ApiProperty()
  name: string;
}
