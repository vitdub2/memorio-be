import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { DeviceService } from './device.service';

@Injectable()
export class DeviceGuard implements CanActivate {
  constructor(private readonly deviceService: DeviceService) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const request = context.switchToHttp().getRequest();
    const userId = request.user._id;
    const deviceId = request.params.id;
    try {
      const task = await this.deviceService.getDevice(deviceId);
      const id = task.userId.toString();
      if (id === userId) {
        return true;
      }
    } catch (e) {
      throw new BadRequestException('Invalid ID');
    }
  }
}
