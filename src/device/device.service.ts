import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Device, DeviceDocument } from '../schema/device.schema';
import { InjectModel } from '@nestjs/mongoose';
import { DeviceDto } from './device.interfaces';
import { IToken } from '../auth/auth.interfaces';
import { MONGO_REPO_OPTIONS } from '../utils/constants';

@Injectable()
export class DeviceService {
  constructor(
    @InjectModel(Device.name)
    private readonly deviceModel: Model<DeviceDocument>,
  ) {}

  getUserDevices(userId: string): Promise<Device[]> {
    return this.deviceModel.find({ userId }).exec();
  }

  getDevice(deviceId: string): Promise<Device> {
    return this.deviceModel.findById(deviceId).exec();
  }

  createDevice(deviceDto: DeviceDto, userId: string): Promise<Device> {
    return new this.deviceModel({ ...deviceDto, userId }).save();
  }

  updateDevice(deviceDto: DeviceDto, deviceId: string): Promise<Device> {
    return this.deviceModel
      .findByIdAndUpdate(deviceId, deviceDto, MONGO_REPO_OPTIONS)
      .exec();
  }

  updateToken(token: IToken, deviceId: string): Promise<Device> {
    return this.deviceModel
      .findByIdAndUpdate(deviceId, token, MONGO_REPO_OPTIONS)
      .exec();
  }

  deleteDevice(deviceId: string): Promise<Device> {
    return this.deviceModel
      .findByIdAndDelete(deviceId, MONGO_REPO_OPTIONS)
      .exec();
  }
}
