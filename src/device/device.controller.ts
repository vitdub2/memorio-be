import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { MODULE_NAMES } from '../utils/constants';
import { ApiTags } from '@nestjs/swagger';
import { UserID } from '../utils/user-id.decorator';
import { Device } from '../schema/device.schema';
import { DeviceService } from './device.service';
import { ValidationPipe } from '../utils/validation.pipe';
import { DeviceDto } from './device.interfaces';
import { DeviceGuard } from './device.guard';
import { IToken } from '../auth/auth.interfaces';

@Controller(MODULE_NAMES.DEVICES)
@ApiTags(MODULE_NAMES.DEVICES)
export class DeviceController {
  constructor(private readonly deviceService: DeviceService) {}

  @Get()
  getDevices(@UserID() userId: string): Promise<Device[]> {
    return this.deviceService.getUserDevices(userId);
  }

  @Post()
  createDevice(
    @UserID() userId: string,
    @Body(new ValidationPipe()) deviceDto: DeviceDto,
  ): Promise<Device> {
    return this.deviceService.createDevice(deviceDto, userId);
  }

  @Put(':id/token')
  @UseGuards(DeviceGuard)
  updateDeviceToken(
    @Body(new ValidationPipe()) token: IToken,
    @Param('id') deviceId: string,
  ): Promise<Device> {
    return this.deviceService.updateToken(token, deviceId);
  }

  @Put(':id')
  @UseGuards(DeviceGuard)
  updateDevice(
    @Body(new ValidationPipe()) deviceDto: DeviceDto,
    @Param('id') deviceId: string,
  ): Promise<Device> {
    return this.deviceService.updateDevice(deviceDto, deviceId);
  }

  @Delete(':id')
  @UseGuards(DeviceGuard)
  deleteDevice(@Param('id') deviceId: string): Promise<Device> {
    return this.deviceService.deleteDevice(deviceId);
  }
}
