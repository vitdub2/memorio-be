import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task, TaskDocument } from '../schema/task.schema';
import { TaskDto } from './task.interfaces';
import { PushService } from '../push/push.service';
import { forEach } from 'lodash';
import { MONGO_REPO_OPTIONS } from '../utils/constants';

@Injectable()
export class TaskService {
  constructor(
    @InjectModel(Task.name) private taskModel: Model<TaskDocument>,
    private readonly pushService: PushService,
  ) {
    this.getAllTasks().then((tasks) => {
      forEach(tasks, (task: Task) => {
        this.pushService.createCronJobsForTask(task);
      });
    });
  }

  async getAllTasks(): Promise<Task[]> {
    return this.taskModel.find().exec();
  }

  async getAllTasksForUser(userId: string): Promise<Task[]> {
    return this.taskModel.find({ userId }).exec();
  }

  async getTask(taskId: string): Promise<Task> {
    return this.taskModel.findById(taskId).exec();
  }

  async createTask(taskDto: TaskDto, userId: string): Promise<Task> {
    const task = await new this.taskModel({ ...taskDto, userId }).save();
    this.pushService.createCronJobsForTask(task);
    return task;
  }

  async updateTask(taskDto: TaskDto, taskId: string): Promise<Task> {
    const oldTask = await this.getTask(taskId);
    this.pushService.deleteCronJobsForTask(oldTask);
    const task = await this.taskModel
      .findByIdAndUpdate(taskId, taskDto, MONGO_REPO_OPTIONS)
      .exec();
    this.pushService.createCronJobsForTask(task);
    return task;
  }

  async deleteTask(taskId: string): Promise<Task> {
    const task = await this.taskModel
      .findByIdAndDelete(taskId, { useFindAndModify: false })
      .exec();
    this.pushService.deleteCronJobsForTask(task);
    return task;
  }
}
