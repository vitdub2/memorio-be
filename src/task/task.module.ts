import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { SchemasModule } from '../schema/schemas.module';
import { TaskController } from './task.controller';
import { PushModule } from '../push/push.module';

@Module({
  imports: [SchemasModule, PushModule],
  providers: [TaskService],
  controllers: [TaskController],
  exports: [TaskService],
})
export class TaskModule {}
