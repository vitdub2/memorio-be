import { IsArray, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Optional } from '@nestjs/common';

export class TaskDto {
  @IsString()
  @ApiProperty()
  name: string;

  @Optional()
  @IsString()
  @ApiProperty()
  desc: string;

  @IsArray()
  @ApiProperty()
  notificationTimes: string[];
}
