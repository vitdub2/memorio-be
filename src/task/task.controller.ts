import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { MODULE_NAMES } from '../utils/constants';
import { ApiTags } from '@nestjs/swagger';
import { TaskService } from './task.service';
import { UserID } from '../utils/user-id.decorator';
import { Task } from '../schema/task.schema';
import { TaskGuard } from './task.guard';
import { ValidationPipe } from '../utils/validation.pipe';
import { UserCreateDto } from '../user/user.interfaces';
import { TaskDto } from './task.interfaces';

@Controller(MODULE_NAMES.TASKS)
@ApiTags(MODULE_NAMES.TASKS)
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get()
  async getUserTasks(@UserID() userId: string): Promise<Task[]> {
    return this.taskService.getAllTasksForUser(userId);
  }

  @Get(':id')
  @UseGuards(TaskGuard)
  async getUserTask(@Param('id') taskId: string): Promise<Task> {
    return this.taskService.getTask(taskId);
  }

  @Post()
  async createTask(
    @Body(new ValidationPipe()) taskDto: TaskDto,
    @UserID() userId: string,
  ): Promise<Task> {
    return this.taskService.createTask(taskDto, userId);
  }

  @Put(':id')
  @UseGuards(TaskGuard)
  async editTask(
    @Body(new ValidationPipe()) taskDto: TaskDto,
    @Param('id') taskId: string,
  ): Promise<Task> {
    return this.taskService.updateTask(taskDto, taskId);
  }

  @Delete(':id')
  @UseGuards(TaskGuard)
  async deleteTask(@Param('id') taskId: string): Promise<Task> {
    return this.taskService.deleteTask(taskId);
  }
}
