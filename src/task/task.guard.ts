import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { TaskService } from './task.service';

@Injectable()
export class TaskGuard implements CanActivate {
  constructor(private readonly taskService: TaskService) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const request = context.switchToHttp().getRequest();
    const userId = request.user._id;
    const taskId = request.params.id;
    try {
      const task = await this.taskService.getTask(taskId);
      const id = task.userId.toString();
      if (id === userId) {
        return true;
      }
    } catch (e) {
      throw new BadRequestException('Invalid ID');
    }
  }
}
