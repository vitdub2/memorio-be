import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ValidationPipe } from '../utils/validation.pipe';
import { EmailDto, UserCreateDto, UserDto } from './user.interfaces';
import { User } from '../schema/user.schema';
import { Public } from '../utils/public.decorator';
import { UserID } from '../utils/user-id.decorator';
import { ApiTags } from '@nestjs/swagger';
import { MODULE_NAMES } from '../utils/constants';

@Controller(MODULE_NAMES.USERS)
@ApiTags(MODULE_NAMES.USERS)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get(':id')
  async getUserById(@Param('id') id: string): Promise<User> {
    return this.userService.findUserById(id);
  }

  @Post()
  @Public()
  async createUser(
    @Body(new ValidationPipe()) userDto: UserCreateDto,
  ): Promise<EmailDto> {
    return this.userService.createUser(userDto);
  }

  @Put()
  async updateUser(
    @UserID() userId,
    @Body(new ValidationPipe()) userDto: UserDto,
  ): Promise<User> {
    return this.userService.updateUser(userId, userDto);
  }

  @Delete()
  async deleteUser(@UserID() userId: string): Promise<User> {
    return this.userService.deleteUser(userId);
  }
}
