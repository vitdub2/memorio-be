export const passwordCutter = (user: any) => {
  const trueUser = user['_doc'];
  delete trueUser.password;
  delete trueUser.confirmCode;
  return trueUser;
};
