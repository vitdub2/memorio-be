import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @IsString()
  @ApiProperty()
  username: string;

  @IsString()
  @ApiProperty()
  email: string;
}

export class UserCreateDto extends UserDto {
  @IsString()
  @ApiProperty()
  password: string;
}

export enum UserState {
  UNCONFIRMED = 'UNCONFIRMED',
  CONFIRMED = 'CONFIRMED',
}

export interface EmailDto {
  email: string;
}
