import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { SchemasModule } from '../schema/schemas.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [SchemasModule, MailModule],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
