import { Injectable, UnauthorizedException } from '@nestjs/common';
import { EmailDto, UserCreateDto, UserDto, UserState } from './user.interfaces';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../schema/user.schema';
import { Model } from 'mongoose';
import { passwordCutter } from './password.cutter';
import * as bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';
import { MailService } from '../mail/mail.service';
import { IPassword } from '../auth/auth.interfaces';
import { MONGO_REPO_OPTIONS } from '../utils/constants';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private readonly mailService: MailService,
  ) {}
  async findUserByConfirmCode(confirmCode: string): Promise<User> {
    return this.userModel.findOne({ confirmCode }).exec();
  }

  async findUserByLogin(login: string): Promise<User> {
    if (login.includes('@')) return this.userModel.findOne({ email: login });
    return this.userModel.findOne({ username: login });
  }

  async findUserById(id: string): Promise<User> {
    return passwordCutter(await this.userModel.findById(id).exec());
  }

  async createUser(userDto: UserCreateDto): Promise<any> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(userDto.password, salt);
    try {
      const user = await new this.userModel({
        ...userDto,
        password,
        state: UserState.UNCONFIRMED,
        confirmCode: uuidv4(),
      });
      await user.save();
      await this.mailService.sendEmail({
        to: user.email,
        subject: 'Confirm email',
        link: `confirm/${user.confirmCode}`,
        templateName: 'confirm_email',
      });
      return {
        email: userDto.email,
      };
    } catch (e) {
      console.log(e);
      throw new UnauthorizedException(
        'Пользователь с таким логином/email уже зарегистрирован',
      );
    }
  }

  async updateUser(id: string, userDto: UserDto): Promise<User> {
    return passwordCutter(
      await this.userModel
        .findOneAndUpdate({ _id: id }, userDto, MONGO_REPO_OPTIONS)
        .exec(),
    );
  }

  async setActive(id: string, isActive: boolean): Promise<User> {
    return passwordCutter(
      await this.userModel
        .findByIdAndUpdate(id, { isActive }, MONGO_REPO_OPTIONS)
        .exec(),
    );
  }

  regenerateConfirmCode(id: string) {
    return this.userModel
      .findByIdAndUpdate(
        id,
        {
          confirmCode: uuidv4(),
          state: UserState.CONFIRMED,
        },
        MONGO_REPO_OPTIONS,
      )
      .exec();
  }

  async setNewPasswordForUser(id: string, passwordDto: IPassword) {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(passwordDto.password, salt);
    return this.userModel
      .findByIdAndUpdate(
        id,
        {
          confirmCode: uuidv4(),
          state: UserState.CONFIRMED,
          password,
        },
        MONGO_REPO_OPTIONS,
      )
      .exec();
  }

  async deleteUser(id: string): Promise<User> {
    return passwordCutter(
      await this.userModel.findByIdAndDelete(id, MONGO_REPO_OPTIONS).exec(),
    );
  }
}
