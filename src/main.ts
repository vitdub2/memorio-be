import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as admin from 'firebase-admin';
import * as dotenv from 'dotenv';
import { AppModule } from './app/app.module';

dotenv.config({ path: '.env' });

async function createNestServer() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const config = new DocumentBuilder()
    .setTitle('Motivator')
    .setDescription('Motivator API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  const cert = {
    clientEmail: process.env.CLIENT_EMAIL,
    projectId: process.env.PROJECT_ID,
    privateKey: process.env.PRIVATE_KEY,
  };
  admin.initializeApp({
    credential: admin.credential.cert(cert as any),
    databaseURL: process.env.FIREBASE_URL,
  });

  app.enableCors();

  await app.listen(3000);
}

createNestServer();
