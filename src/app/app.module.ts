import { Global, Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { ScheduleModule } from '@nestjs/schedule';
import { TaskModule } from '../task/task.module';
import { DeviceModule } from '../device/device.module';
import { PushModule } from '../push/push.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { MailModule } from '../mail/mail.module';
import * as dotenv from 'dotenv';
import { AppController } from './app.controller';
dotenv.config({ path: '.env' });

@Global()
@Module({
  imports: [
    AuthModule,
    UserModule,
    TaskModule,
    DeviceModule,
    MongooseModule.forRoot(process.env.MONGO_URL),
    ScheduleModule.forRoot(),
    PushModule,
    MailerModule.forRoot({
      transport: process.env.SMTP_SETTINGS,
      defaults: {
        from: 'Motivator',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new PugAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    MailModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
