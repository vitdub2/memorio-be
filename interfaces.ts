export interface Message {
  body: string;
}

export interface WebsocketAuthInfo {
  userToken: string;
  deviceId: string;
}
