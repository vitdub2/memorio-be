#!/bin/bash

# any future command that fails will exit the script

echo "Delete the old repo"
rm -rf memorio-be

echo "Killing proccess on port 3000"
sudo kill -9 $(sudo lsof -t -i:3000)

echo "Installing Node"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 14.16.0

echo "Installing Rimraf"
npm install -g rimraf

echo "Installing Nest"
npm install -g @nestjs/cli

# clone the repo again
git clone https://gitlab.com/vitdub2/memorio-be.git

echo "Copying .env"
cp .env memorio-be

cd memorio-be

echo "Running npm install"
npm i

set -e

echo "Running npm build"
npm run build

echo "Restarting the server"
(npm run start:prod&)

exit
